package com.learn.kafkabyibe.kafkaproducer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.mongodb.lang.NonNull;

@Component
public final class KafkaProducer {
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);
	
	@Autowired
	private final KafkaTemplate<String, String> kafkaTemplate;
	
	public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}
	
	public void sendMessage(String message) {
		String topicName = "test_hola";
		
		ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topicName, message);
		future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
			@Override
			public void onFailure(@NonNull Throwable throwable) {
				logger.error("Failed to send message", throwable);
			}
			
			@Override
			public void onSuccess(SendResult<String, String> stringStringSendResult) {
				logger.info("Message sent successfully");
			}
		});
	}
}
