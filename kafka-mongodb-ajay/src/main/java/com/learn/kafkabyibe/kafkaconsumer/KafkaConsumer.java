package com.learn.kafkabyibe.kafkaconsumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.learn.kafkabyibe.model.Role;
import com.learn.kafkabyibe.repository.MongoDBRepository;

@Service
public final class KafkaConsumer {
	
	private final MongoDBRepository mongodbRepository;
	public KafkaConsumer(MongoDBRepository mongodbRepository) {
		this.mongodbRepository = mongodbRepository;
	}
	
	@KafkaListener(topics = "test_hola", containerFactory = "kafkfaListenerContainerFactory")
	public void consumeJson(String message) {
		Role role = new Role();
		role.setRoleName(message);
		
		mongodbRepository.insert(role);
	}
}
