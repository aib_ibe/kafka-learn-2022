package com.learn.kafkabyibe.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.learn.kafkabyibe.model.Role;

public interface MongoDBRepository extends MongoRepository<Role, String>{

}
