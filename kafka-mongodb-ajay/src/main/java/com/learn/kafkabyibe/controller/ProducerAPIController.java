package com.learn.kafkabyibe.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learn.kafkabyibe.kafkaproducer.KafkaProducer;
import com.learn.kafkabyibe.model.Role;
import com.learn.kafkabyibe.repository.MongoDBRepository;

@RestController
@RequestMapping("/api")
public class ProducerAPIController {

		@Autowired
		private final MongoDBRepository mongodbRepository;
		private final KafkaProducer kafkaProducer;
		
		public ProducerAPIController(MongoDBRepository mongodbRepository, KafkaProducer kafkaProducer) {
			this.mongodbRepository = mongodbRepository;
			this.kafkaProducer = kafkaProducer;
		}
		
		@GetMapping(value = "/viewall")
		public List<Role> getRoles(){
			return mongodbRepository.findAll();
		}
		
		@GetMapping(value = "/addrole/{newrole}")
		public String addRole(@PathVariable("newrole") String newrole) {
			kafkaProducer.sendMessage(newrole);
			return "Message sent succesfully";
		}
}
