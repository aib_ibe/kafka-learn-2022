package com.learn.kafkabyibe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class KafkaByIbeApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaByIbeApplication.class, args);
	}

}
