package learn.kafka.csvtokafka.csvreader;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

import com.csvreader.CsvReader;

public class CsvFileReader implements Supplier<FiveThousandDatas>{
	
	private CsvReader csvReader;
	private final String filePath;
	
	public CsvFileReader(String filePath) throws IOException {
		
		this.filePath = filePath;
		
		try {
			csvReader = new CsvReader(filePath);
			csvReader.readHeaders();
		} catch (IOException e) {
			throw new IOException("Error reading file from file : " + filePath, e);
		}
	}
	
	@Override
	public FiveThousandDatas get() {
		
		FiveThousandDatas fiveThousandDatas = null; 
		
		try {
			if(csvReader.readRecord()) {
				csvReader.getRawRecord();
				fiveThousandDatas = new FiveThousandDatas(csvReader.get(0), csvReader.get(1), csvReader.get(2), csvReader.get(3), csvReader.get(4));
			}
		} catch (IOException e) {
			 throw new NoSuchElementException("IOException from " + filePath);
		}
		
		 if (null==fiveThousandDatas) {
	            throw new NoSuchElementException("All records read from " + filePath);
	        }
		
		return fiveThousandDatas;
	}

}
