package learn.kafka.csvtokafka.csvreader;

import com.fasterxml.jackson.annotation.JsonFormat;

//Json Format buat model / template dari data yang nantinya dibaca dari csv
public class FiveThousandDatas {
	
	@JsonFormat
	private String kolom_1;
	
	@JsonFormat
	private String kolom_2;
	
	@JsonFormat
	private String kolom_3;
	
	@JsonFormat
	private String kolom_4;
	
	@JsonFormat
	private String kolom_5;
	
	public FiveThousandDatas() {
	}
	
	public FiveThousandDatas(String kolom_1, String kolom_2, String kolom_3, String kolom_4, String kolom_5) {
		this.kolom_1 = kolom_1;
		this.kolom_2 = kolom_2;
		this.kolom_3 = kolom_3;
		this.kolom_4 = kolom_4;
		this.kolom_5 = kolom_5;
	}
}
