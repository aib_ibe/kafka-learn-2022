package learn.kafka.csvtokafka;

import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import learn.kafka.csvtokafka.csvreader.CsvFileReader;
import learn.kafka.csvtokafka.producer.KafkaProducer;

@SpringBootApplication
public class CsvToKafkaApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CsvToKafkaApplication.class, args);
		
		// file path dari file csv
		String filePath = "D:\\Codes\\Learn\\Data5000.csv"; 
		
		// nama topic
		String topic = "cinco-mil";
		
		// alamat broker
		String broker = "127.0.0.1:9092";
		
		Stream.generate(new CsvFileReader(filePath)).sequential().forEachOrdered(new KafkaProducer(topic, broker));
	}

}
