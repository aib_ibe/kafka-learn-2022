package learn.kafka.csvtokafka.producer;

import java.util.Properties;
import java.util.function.Consumer;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;

import learn.kafka.csvtokafka.csvreader.FiveThousandDatas;
import learn.kafka.csvtokafka.serializer.JsonSerializer;

public class KafkaProducer implements Consumer<FiveThousandDatas>{
	
	private final String topic; 
	private final org.apache.kafka.clients.producer.KafkaProducer<byte[], byte[]> producer;
	private final JsonSerializer<FiveThousandDatas> serializer;
	
	public KafkaProducer(String kafkaTopic, String kafkaBrokers) {
		this.topic = kafkaTopic;
		this.producer = new org.apache.kafka.clients.producer.KafkaProducer<>(createKafkaProperties(kafkaBrokers));
		this.serializer = new JsonSerializer<>();
	}

	@Override
	public void accept(FiveThousandDatas record) {
		
		byte[] data = serializer.toJSONBytes(record);
		ProducerRecord<byte[], byte[]> kafkaRecord = new ProducerRecord<>(topic, data);
		producer.send(kafkaRecord);
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static Properties createKafkaProperties(String brokers) {
		Properties kafkaProps = new Properties();
		kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
		kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getCanonicalName());
		kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getCanonicalName());
		return kafkaProps;
	}
	
}
